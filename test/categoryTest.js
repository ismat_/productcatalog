let Category = require('../api/models/category'),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  server = require("../server"),
  should = chai.should();

chai.use(chaiHttp);

describe("Categories", function () {
  beforeEach((done) => {
    Category.deleteMany({}, (err) => {
      done();
    });
  });

  beforeEach((done) => {
    Category.create({ "name": "Shoes", "description": "Designed for use in urban areas" })
      .then(() => done());
  });

  describe('/GET category', () => {
    it('it should GET all the categories', (done) => {
      chai.request(server)
        .get('/categories')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(1);
          done();
        });
    });
  });


  describe('/POST category', () => {
    it('it should POST a category ', (done) => {
      let book = {
        name: "book",
        description: "Science Fiction"
      }
      chai.request(server)
        .post('/categories')
        .send(book)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Category successfully added');
          res.body.category.should.have.property('name');
          res.body.category.should.have.property('description');
          done();
        });
    });
  });

  describe('/PATCH/:id category', () => {
    it('it should UPDATE a category given the id', (done) => {
      let category = new Category({ name: "book", description: "Horro" })
      category.save((err, category) => {
        chai.request(server)
          .patch('/categories/' + category._id)
          .send({ description: "Horror" })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Category updated successfully');
            res.body.category.should.have.property('description').eql("Horror");
            done();
          });
      });
    });
  });


  describe('/DELETE/:id category', () => {
    it('it should DELETE a category given the id', (done) => {
      let category = new Category({ name: "book", description: "Horro" });
      category.save((err, category) => {
        chai.request(server)
          .delete('/categories/' + category.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Category deleted successfully');
            res.body.result.should.have.property('ok').eql(1);
            res.body.result.should.have.property('n').eql(1);
            done();
          });
      });
    });
  });
});