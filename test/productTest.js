let mongoose = require("mongoose").model,
  Product = require('../api/models/product'),
  Category = require('../api/models/category'),
  assert = require("assert"),
  chai = require("chai"),
  chaiHttp = require("chai-http"),
  server = require("../server");

let should = chai.should(); //
chai.use(chaiHttp);

describe("Products", function () {
  beforeEach((done) => {
    Category.deleteMany({}, (err) => {
      done();
    });
  });

  describe('CRUD operations on product', () => {
    it('should GET all the products in a given category', (done) => {

      let category = new Category({ name: "book", description: "Horror" }),
        product1 = new Product({
          title: "The Shining",
          description: "Interesting book",
          quantity: 56,
          category: category.id
        }),
        product2 = new Product({
          title: "The Hunger",
          description: "Awasome book",
          quantity: 143,
          category: category.id
        });


      category.save((err, category) => {
        product1.save();
        product2.save();
        chai.request(server)
          .get('/categories/' + category.id + '/products')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eql(2);
            done();
          });
      });
    });

    it('should POST a product in a given category', (done) => {
      let category = new Category({ name: "book", description: "Horror" }),
        product = {
          title: "Dracula",
          description: "Interesting book",
          quantity: 347,
          category: category.id
        };

      category.save((err, category) => {
        chai.request(server)
          .post('/categories/' + category.id + '/products')
          .send(product)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Product successfully created');
            res.body.product.should.have.property('title').eql('Dracula');
            res.body.product.should.have.property('description');
            done();
          });

      });
    });


    it('should UPDATE a product in a given category with the given id', (done) => {
      let category = new Category({ name: "book", description: "Horror" });
      let product = new Product({
        title: "D",
        description: "Interesting book",
        quantity: 347,
        category: category.id
      });
      category.save((err, category) => {
        product.save((err, product) => {
          chai.request(server)
            .patch('/categories/' + category._id + '/products/' + product.id)
            .send({ title: "Dune" })
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message').eql('Product successfully updated');
              res.body.product.should.have.property('title').eql("Dune");
              done();
            });
        })
      });
    });

    it('should DELETE a product in a given category with the given id', (done) => {
      let category = new Category({
        name: "Vehicle",
        description: "Urban land use"
      }),
        product = new Product({
          title: "Car",
          brand: "Acura ",
          description: "vehicle division of Japanese automaker Honda",
          quantity: 234,
          category: category.id
        })

      category.save((err, category) => {
        product.save((err, product) => {
          chai.request(server)
            .delete('/categories/' + category._id + '/products/' + product._id)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message').eql('Product deleted successfully');
              res.body.result.should.have.property('ok').eql(1);
              res.body.result.should.have.property('n').eql(1);
              done();
            });
        })
      })

    });
  });
});
