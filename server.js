var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Product = require('./api/models/product'),
  Category = require('./api/models/category'),
  bodyParser = require('body-parser'),
  apicache = require('apicache');

var cache = apicache.middleware;
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/productCatalog', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(cache('5 minutes'));


var routes = require('./api/routes/appRoutes');
routes(app);

app.use(function (req, res) {
  res.status(404).send({ url: req.originalUrl + ' not found' })
});

app.listen(port);

module.exports = app;
console.log('...Listening on port: ' + port);
