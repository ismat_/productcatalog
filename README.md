# Product Catalog

### Nodejs and mongoDb has to be installed and setup before using this app. 
### It can be installed through the following links
* https://nodejs.org/en/download/package-manager/
* https://docs.mongodb.com/manual/installation/

### Installations
* npm install

### Test
* npm run test

### Note: Before running the app test has to be stopped using Ctrl + C
* npm run start