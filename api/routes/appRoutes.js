'use strict';

module.exports = function (app) {
	var category = require('../controllers/categoryController'),
		product = require('../controllers/productController');

	app.route('/categories')
		.get(category.list_all_categories)
		.post(category.create_a_category);

	app.route('/categories/:categoryId')
		.patch(category.update_a_category)
		.delete(category.delete_a_category);

	app.route('/categories/:categoryId/products')
		.get(product.list_all_products_by_category)
		.post(product.create_a_product);

	app.route('/categories/:categoryId/products/:productId')
		.patch(product.update_a_product)
		.delete(product.delete_a_product);
};
