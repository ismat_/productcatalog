'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Category = new Schema({
  name: {
    type: String,
    Required: 'Enter the category name'
  },
  description: String
});


module.exports = mongoose.model('Category', Category);