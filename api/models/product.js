'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Product = new Schema({
  title: {
    type: String,
    Required: 'Enter the product title'
  },
  brand: String,
  description: {
    type: String,
    Required: 'Please enter product description'
  },
  quantity: {
    type: Number,
    Required: 'Please specify the product quantity'
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: "Category"
  }
});


module.exports = mongoose.model('Product', Product);