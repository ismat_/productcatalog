var mongoose = require('mongoose'),
  Product = mongoose.model('Product');


exports.create_a_product = function (req, res) {
  var new_product = new Product({ ...req.body, category: req.params.categoryId });
  new_product.save(function (err, product) {
    if (err)
      res.send(err);
    res.json({message: 'Product successfully created', product});
  });
};

exports.list_all_products_by_category = (req, res) => {
  const categoryId = req.params.categoryId;
  Product.find({ category: categoryId }, (err, products) => {
    if (err)
      res.send(err);
    res.json(products);
  })
    .populate("category", "name description -_id")
    .select("-__v");
};

exports.update_a_product = (req, res) => {
  const productId = req.params.productId;
  Product.findOneAndUpdate({ _id: productId }, req.body, { new: true }, (err, product) => {
    if (err)
      res.send(err);
    res.json({message: "Product successfully updated", product});
  })
    .populate("category", "name description -_id")
    .select("-__v");
};


exports.delete_a_product = function (req, res) {
  Product.deleteOne({
    _id: req.params.productId
  }, function (err, result) {
    if (err)
      res.send(err);
    res.json({ message: 'Product deleted successfully', result });
  });
};