'use strict';

var mongoose = require('mongoose'),
  Category = mongoose.model('Category'),
  Product = mongoose.model('Product');


exports.create_a_category = function (req, res) {
  var new_category = new Category(req.body);
  new_category.save(function (err, category) {
    if (err)
      res.send(err);
    res.json({
      message: 'Category successfully added', category
    });
  });
};

exports.update_a_category = function (req, res) {
  Category.findOneAndUpdate({ _id: req.params.categoryId }, req.body, { new: true }, (err, category) => {
    if (err)
      res.send(err);
    res.json({ message: 'Category updated successfully', category });
  });
};

exports.list_all_categories = function (req, res) {
  Category.find({}, function (err, category) {
    if (err)
      res.send(err);
    res.json(category);
  });
};

exports.delete_a_category = function (req, res) {
  Category.deleteOne({
    _id: req.params.categoryId
  }, function (err, result) {
    if (err)
      res.send(err);
    res.json({ message: 'Category deleted successfully', result });
  });
};
